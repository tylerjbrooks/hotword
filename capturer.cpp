/*
 * Copyright © 2019 Tyler J. Brooks <tylerjbrooks@digispeaker.com> <https://www.digispeaker.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <iostream>
#include <fstream>

#include "capturer.h"

namespace hotword {

Capturer::Capturer(unsigned int yieldtime) 
  : Base(yieldtime) {
}

Capturer::~Capturer() {
}

std::unique_ptr<Capturer> Capturer::create(unsigned int yield_time,
    std::string& device, unsigned int channels, const char* model_path, 
    const char* labels_path, float sensitivity) {
  auto obj = std::unique_ptr<Capturer>(new Capturer(yield_time));
  obj->init(device, channels, model_path, labels_path, sensitivity);
  return obj;
}

bool Capturer::init(std::string& device, unsigned int channels, 
    const char* model_path, const char* labels_path, float sensitivity) { 

  device_ = device;
  channels_ = channels;
  model_path_ = model_path;
  labels_path_ = labels_path;
  sensitivity_ = sensitivity;

  sample_rate_ = 16000;
  format_ = SND_PCM_FORMAT_S16_LE;

  handle_ = nullptr;

#ifdef CAPTURE_RAW_DATA
  fd_raw_ = nullptr;
#endif

  stream_on_ = false;

 return true;
}

#ifdef CAPTURE_RAW_DATA
static const char* pcmFormatToStr(snd_pcm_format_t fmt) {
  switch (fmt) {
    case SND_PCM_FORMAT_UNKNOWN:    return "unknown";
    case SND_PCM_FORMAT_S8:         return "s8";
    case SND_PCM_FORMAT_U8:         return "u8";
    case SND_PCM_FORMAT_S16_LE:     return "s16_le";
    case SND_PCM_FORMAT_S16_BE:     return "s16_be";
    case SND_PCM_FORMAT_U16_LE:     return "u16_le";
    case SND_PCM_FORMAT_U16_BE:     return "u16_be";
    case SND_PCM_FORMAT_S24_LE:     return "s24_le";
    case SND_PCM_FORMAT_S24_BE:     return "s24_be";
    case SND_PCM_FORMAT_U24_LE:     return "u24_le";
    case SND_PCM_FORMAT_U24_BE:     return "u24_be";
    case SND_PCM_FORMAT_S32_LE:     return "s32_le";
    case SND_PCM_FORMAT_S32_BE:     return "s32_be";
    case SND_PCM_FORMAT_U32_LE:     return "u32_le";
    case SND_PCM_FORMAT_U32_BE:     return "u32_be";
    case SND_PCM_FORMAT_FLOAT_LE:   return "float_le";
    case SND_PCM_FORMAT_FLOAT_BE:   return "float_be";
    case SND_PCM_FORMAT_FLOAT64_LE: return "float64_le";
    case SND_PCM_FORMAT_FLOAT64_BE: return "float64_be";
    case SND_PCM_FORMAT_IEC958_SUBFRAME_LE: return "iec958_subframe_le";
    case SND_PCM_FORMAT_IEC958_SUBFRAME_BE: return "iec958_subframe_be";
    case SND_PCM_FORMAT_MU_LAW:     return "mu_law";
    case SND_PCM_FORMAT_A_LAW:      return "a_law";
    case SND_PCM_FORMAT_IMA_ADPCM:  return "ima_adpcm";
    case SND_PCM_FORMAT_MPEG:       return "mpeg";
    case SND_PCM_FORMAT_GSM:        return "gsm";
    case SND_PCM_FORMAT_SPECIAL:    return "special";
    case SND_PCM_FORMAT_S24_3LE:    return "s24_3le";
    case SND_PCM_FORMAT_S24_3BE:    return "s24_3be";
    case SND_PCM_FORMAT_U24_3LE:    return "u24_3le";
    case SND_PCM_FORMAT_U24_3BE:    return "u24_3be";
    case SND_PCM_FORMAT_S20_3LE:    return "s20_3le";
    case SND_PCM_FORMAT_S20_3BE:    return "s20_3be";
    case SND_PCM_FORMAT_U20_3LE:    return "u20_3le";
    case SND_PCM_FORMAT_U20_3BE:    return "u20_3be";
    case SND_PCM_FORMAT_S18_3LE:    return "s18_3le";
    case SND_PCM_FORMAT_S18_3BE:    return "s18_3be";
    case SND_PCM_FORMAT_U18_3LE:    return "u18_3le";
    case SND_PCM_FORMAT_U18_3BE:    return "u18_3be";
    case SND_PCM_FORMAT_G723_24:    return "g723_24";
    case SND_PCM_FORMAT_G723_24_1B: return "g723_24_1b";
    case SND_PCM_FORMAT_G723_40:    return "g723_40";
    case SND_PCM_FORMAT_G723_40_1B: return "g723_40_1b";
    case SND_PCM_FORMAT_DSD_U8:     return "dsd_u8";
    case SND_PCM_FORMAT_DSD_U16_LE: return "dsd_u16_le";
    case SND_PCM_FORMAT_DSD_U32_LE: return "dsd_u32_le";
    case SND_PCM_FORMAT_DSD_U16_BE: return "dsd_u16_be";
    case SND_PCM_FORMAT_DSD_U32_BE: return "dsd_u32_be";
  }

  return "unknown";
}
#endif

bool Capturer::waitingToRun() {
 
  if (!stream_on_) {

    differ_tot_.begin();

    // read labels file
    dbgMsg("read labels file\n");
    std::ifstream ifs(labels_path_.c_str(), std::ifstream::in);
    if (!ifs) {
      dbgMsg("could not open labels file\n");
    }
    std::string line;
    while (std::getline(ifs, line)) {
      labels_.emplace_back(line);
    }

    // create recognizer
    dbgMsg("create recognizer\n");
    recognizer_ = create_audio_recognition(model_path_.c_str());
    SetSensitivity(recognizer_, sensitivity_);

    // create extractor
    dbgMsg("create extractor\n");
    extractor_ = create_feature_extractor();

    // compute buffer size
    dbgMsg("compute buffer size\n");
    frame_size_ = snd_pcm_format_width(format_) / 8;
    frames_ = GetInputDataSize(recognizer_) / frame_size_;
    buffer_.resize(frames_ * frame_size_);
    dbgMsg("buffer size: %d\n", frames_ * frame_size_);

    // open device
    dbgMsg("open device\n");
    int err = snd_pcm_open(&handle_, device_.c_str(), 
        SND_PCM_STREAM_CAPTURE, 0);
    if (err < 0) {
      dbgMsg("failed: open device %s (%s)\n", device_.c_str(), snd_strerror(err));
      return false;
    }

    // allocate hw params
    dbgMsg("allocate hw params\n");
    snd_pcm_hw_params_t* hw_params;
    err = snd_pcm_hw_params_malloc(&hw_params);
    if (err < 0) {
      dbgMsg("failed: allocate hw params (%s)\n", snd_strerror(err));
      return false;
    }

    // get hw params
    dbgMsg("get hw params\n");
    err = snd_pcm_hw_params_any(handle_, hw_params);
    if (err < 0) {
      dbgMsg("failed: get hw params (%s)\n", snd_strerror(err));
      return false;
    }

    // set read access
    dbgMsg("set read access\n");
    err = snd_pcm_hw_params_set_access(handle_, hw_params, 
        SND_PCM_ACCESS_RW_INTERLEAVED);
    if (err < 0) {
      dbgMsg("failed: set read access (%s)\n", snd_strerror(err));
      return false;
    }
   
    // set format
    dbgMsg("set format\n");
    err = snd_pcm_hw_params_set_format(handle_, hw_params, format_);
    if (err < 0) {
      dbgMsg("failed: set format (%s)\n", snd_strerror(err));
      return false;
    }
   
    // set sample rate
    dbgMsg("set sample rate\n");
    err = snd_pcm_hw_params_set_rate_near(handle_, hw_params, 
        &sample_rate_, 0);
    if (err < 0) {
      dbgMsg("failed: set sample rate (%s)\n", snd_strerror(err));
      return false;
    }
   
    // set channels
    dbgMsg("set channels\n");
    err = snd_pcm_hw_params_set_channels(handle_, hw_params, channels_);
    if (err < 0) {
      dbgMsg("failed: set channels (%s)\n", snd_strerror(err));
      return false;
    }

    // set hw params
    dbgMsg("set hw params\n");
    err = snd_pcm_hw_params(handle_, hw_params);
    if (err < 0) {
      dbgMsg("failed: set hw params (%s)\n", snd_strerror(err));
      return false;
    }
    snd_pcm_hw_params_free (hw_params);

    // prepare device
    dbgMsg("prepare device\n");
    err = snd_pcm_prepare(handle_);
    if (err < 0) {
      dbgMsg("failed: prepare device (%s)\n", snd_strerror(err));
      return false;
    }

#ifdef CAPTURE_RAW_DATA
    char buf[100];
    sprintf(buf, "./pcm_%d.%s", sample_rate_, pcmFormatToStr(format_));
    fd_raw_ = fopen(buf, "wb");
    if (fd_raw_ == nullptr) {
      dbgMsg("failed: open raw frame file\n");
      return false;
    }
#endif

    stream_on_ = true;
  }

  return true;
}

bool Capturer::running() {

  if (stream_on_) {

    // read microphone
    differ_read_.begin();
    int err = snd_pcm_readi(handle_, buffer_.data(), frames_);
    if (err < 0) {
      dbgMsg("failed: read sound\n");
      return false;
    }
    differ_read_.end();

#ifdef CAPTURE_RAW_DATA
    fwrite(buffer_.data(), frame_size_, frames_, fd_raw_);
#endif

    // extract features
    differ_extract_.begin();
    unsigned char melfeatures[800];
    float gain = 1.0;
    int mel_len = signal_to_mel(extractor_, (int16_t*)buffer_.data(), frames_, melfeatures, gain);
    differ_extract_.end();

    // hotword detect
    differ_detect_.begin();
    int res = RunDetection(recognizer_, melfeatures, mel_len);
    if (res > 0) {
      if (static_cast<unsigned int>(res) > labels_.size() ) {
        std::cout << "<detect>";
      } else {
        std::cout << "<" << labels_[res] << ">";
      }
    }
    differ_detect_.end();
  }
  return true;
}

bool Capturer::paused() {
  return true;
}

bool Capturer::waitingToHalt() {

  if (stream_on_) {

    stream_on_ = false;
    snd_pcm_close(handle_);
    differ_tot_.end();

#ifdef CAPTURE_RAW_DATA
    if (fd_raw_) {
      fclose(fd_raw_);
    }
#endif
    
    fprintf(stderr, "\n\nCapturer Results...\n");
    fprintf(stderr, "      read time (us): high:%u avg:%u low:%u reads:%d\n", 
        differ_read_.getHigh_usec(), differ_read_.getAvg_usec(), 
        differ_read_.getLow_usec(),  differ_read_.getCnt());
    fprintf(stderr, "   extract time (us): high:%u avg:%u low:%u reads:%d\n", 
        differ_extract_.getHigh_usec(), differ_extract_.getAvg_usec(), 
        differ_extract_.getLow_usec(),  differ_extract_.getCnt());
    fprintf(stderr, "    detect time (us): high:%u avg:%u low:%u reads:%d\n", 
        differ_detect_.getHigh_usec(), differ_detect_.getAvg_usec(), 
        differ_detect_.getLow_usec(),  differ_detect_.getCnt());
    fprintf(stderr, "     total test time: %f sec\n", 
        differ_tot_.getAvg_usec() / 1000000.f);
    fprintf(stderr, "   frames per second: %f fps\n", 
        differ_read_.getCnt() * 1000000.f / differ_tot_.getAvg_usec());
    fprintf(stderr, "\n");
  }

  return true;
}

} // namespace hotword

