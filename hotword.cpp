/*
 * Copyright © 2019 Tyler J. Brooks <tylerjbrooks@digispeaker.com> <https://www.digispeaker.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <iostream>
#include <memory>
#include <chrono>
#include <signal.h>
#include <unistd.h>

#include "utils.h"
#include "base.h"
#include "capturer.h"

namespace hotword {

std::unique_ptr<Capturer> cap(nullptr);

void usage() {
  std::cout << "hotword -htydcmls" << std::endl;
  std::cout << "version: 0.5"                     << std::endl;
  std::cout                                       << std::endl;
  std::cout << "  where:"                         << std::endl;
  std::cout << "  (h)elp        = this screen"                         << std::endl;
  std::cout << "  (t)esttime    = test duration     (default = 30sec)" << std::endl;
  std::cout << "                = 0 to run until ctrl-c"               << std::endl;
  std::cout << "  (y)ield time  = yield time        (default = 1000usec)" << std::endl;
  std::cout << "  (d)evice      = device name       (default = hw:1,0)"   << std::endl;
  std::cout << "  (c)hannles    = device channels   (default = 1)"        << std::endl;
  std::cout << "  (m)odel       = path to model     (default = ./models/alexa.tflite)" << std::endl;
  std::cout << "  (l)abels      = path to labels    (default = ./models/labelmap.txt)" << std::endl;
  std::cout << "  (s)ensitivity = model sensitivity (default = 0.5)" << std::endl;
}

void quitHandler(int s) {
  if (cap)  { cap->stop(); }

  cap.reset(nullptr);

  exit(1);
}

int main(int argc, char** argv) {

  // defaults
  unsigned int testtime = 30;
  unsigned int yield_time = 1000;
  std::string  device = "hw:1,0";
  unsigned int channels = 1;
  std::string  model  = "./models/alexa.tflite";
  std::string  labels = "./models/alexa_labels.txt";
  float sensitivity = 0.5f;

  // cmd line options
  int c;
  while((c = getopt(argc, argv, "ht:y:d:c:m:l:s:")) != -1) {
    switch (c) {
      case 't': testtime    = std::stoul(optarg); break;
      case 'y': yield_time  = std::stoul(optarg); break;
      case 'd': device      = optarg;             break;
      case 'c': channels    = std::stoul(optarg); break;
      case 'm': model       = optarg;             break;
      case 'l': labels      = optarg;             break;
      case 's': sensitivity = std::stof(optarg);  break;

      case '?':
      case 'h':
      default:  usage(); return 0;
    }
  }

  // ctrl-c handler
  struct sigaction sig_int;
  sig_int.sa_handler = quitHandler;
  sigemptyset(&sig_int.sa_mask);
  sig_int.sa_flags = 0;
  sigaction(SIGINT, &sig_int, NULL);

  // test setup report
  fprintf(stderr, "\nTest Setup...\n");
  if (testtime) {
  fprintf(stderr, "    test time: %d seconds\n", testtime);
  } else {
  fprintf(stderr, "    test time: run until ctrl-c\n");
  }
  fprintf(stderr, "       device: %s\n", device.c_str());
  fprintf(stderr, "     channels: %d\n", channels);
  fprintf(stderr, "        model: %s\n", model.c_str());
  fprintf(stderr, "       lables: %s\n", labels.c_str());
  fprintf(stderr, "  sensitivity: %f\n", sensitivity);
  fprintf(stderr, "\n      pid cmd: top -H -p %d\n\n", getpid());

  // create worker threads
  cap = Capturer::create(yield_time, device, channels,
      model.c_str(), labels.c_str(), sensitivity);

  // start
  dbgMsg("start\n");
  cap->start("cap", 50);

  // run
  dbgMsg("run\n");
  cap->run();

  // run test
  if (testtime) {   // run for testtime...
    for (unsigned int i = 0; i < testtime * 5; i++) {
      fprintf(stderr, "."); 
      fflush(stdout);
      std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
  } else {          // run forever...
    fprintf(stderr, "Hit ctrl-c to terminate...\n\n");
    while (1) {
      fprintf(stderr, ".");
      fflush(stdout);
      std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
  }

  // stop
  dbgMsg("stop\n");
  cap->stop();

  // destroy
  dbgMsg("destroy\n");
  cap.reset(nullptr);

  // done
  dbgMsg("done\n");
  return 0;
}

} // namespace hotword

int main(int argc, char** argv) {
  return hotword::main(argc, argv);
}

