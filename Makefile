# environment variables:
#   RASPBIAN is the location of your raspbian projects
#   RASPBIANCROSS is the location of the raspbian compiler tools
#   TFLOWLITESDK is the location of your 'tensorflow-lite' sdk
#   NYUMAYASDK is the location of your 'nyumaya_audio_recognition' sdk

CC = $(RASPBIANCROSS)gcc
CXX = $(RASPBIANCROSS)g++

EXE = hotword

SRC = hotword.cpp base.cpp capturer.cpp
OBJ = $(SRC:.cpp=.o)

# Turn on 'CAPTURE_ONE_RAW_FRAME' to write the 10th frame
# in to './frame_wxh_ffps.yuv' file (w=width, h=height, f=framerate).
#
# Turn on 'OUTPUT_VARIOUS_BITS_OF_INFO' to print out various
# bits of interesting information about the setup.
#
# Turn on 'DEBUG_MESSAGES' to turn on debug messages.
FEATURES = -DCAPTURE_RAW_DATA -DDEBUG_MESSAGES

CFLAGS = -DSTANDALONE -D__STDC_CONSTANT_MACROS -D__STDC_LIMIT_MACROS 
CFLAGS += -DTARGET_POSIX -D_LINUX -D_REENTRANT -D_LARGEFILE64_SOURCE 
CFLAGS += -D_FILE_OFFSET_BITS=64 -U_FORTIFY_SOURCE 
CFLAGS += $(FEATURES)
CFLAGS += -march=armv7-a -mfpu=neon-vfpv4 -DUSE_NEON 
CFLAGS += -Wall -g -pipe
CFLAGS += -fPIC -DPIC
CFLAGS += -ftree-vectorize -funsafe-math-optimizations 

CXXFLAGS = $(CFLAGS) -std=c++11

INCS = \
	-I. \
	-I$(TFLOWSDK) \
	-I$(TFLOWSDK)/tensorflow/lite/tools/make/downloads/flatbuffers/include \
	-I$(NYUMAYASDK)

LDFLAGS = \
	-L$(TFLOWSDK)/tensorflow/lite/tools/make/gen/rpi_armv7l/lib \
	-L$(NYUMAYASDK)

LIBS = -ltensorflow-lite -lasound -lnyumaya -lpthread -ldl -lrt -lm

$(EXE): $(OBJ)
	$(CXX) $(LDFLAGS) $(OBJ) $(LIBS) -o $@

.cpp.o:
	$(CXX) $(CXXFLAGS) $(INCS) -c $< -o $@

.PHONY: clean
clean:
	rm -f $(EXE) $(OBJ)

