/*
 * Copyright © 2019 Tyler J. Brooks <tylerjbrooks@digispeaker.com> <https://www.digispeaker.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CAPTURER_H
#define CAPTURER_H

#include <cstdio>
#include <string>
#include <vector>
#include <thread>
#include <atomic>

#include <alsa/asoundlib.h>

#include "utils.h"
#include "base.h"

#include "lib.h"

namespace hotword {

class Capturer : public Base {
  public:
    static std::unique_ptr<Capturer> create(unsigned int yield_time, std::string& device,
        unsigned int channels, const char* model, const char* labels, float sensitivity);
    virtual ~Capturer();

  protected:
    Capturer() = delete;
    Capturer(unsigned int yield_time);
    bool init(std::string& device, unsigned int channels, const char* model, 
        const char* labels, float sensitivity);

  protected:
    virtual bool waitingToRun();
    virtual bool running();
    virtual bool paused();
    virtual bool waitingToHalt();

  private:
    std::string device_;
    std::string model_path_;
    std::string labels_path_;
    float sensitivity_;
    unsigned int sample_rate_;
    unsigned int channels_;
    unsigned int frame_size_;
    unsigned int frames_;
    snd_pcm_format_t format_;
    snd_pcm_t* handle_;

    std::vector<unsigned char> buffer_;

    std::vector<std::string> labels_;

    AudioRecognitionImpl* recognizer_;
    FeatureExtractor* extractor_;

    std::atomic<bool> stream_on_;
    Differ differ_read_;
    Differ differ_extract_;
    Differ differ_detect_;
    Differ differ_tot_;

#ifdef CAPTURE_RAW_DATA
    FILE* fd_raw_;
#endif
};

} // namespace hotword

#endif // CAPTURER_H
