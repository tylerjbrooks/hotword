## Hotword

Hotword is hotword detector for the 
[raspberry pi 3b+](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus).  Audio input
is provided by a [USB webcam](https://www.logitech.com/en-us/product/hd-webcam-c615).  The application is written in C++. 

Many thanks to [nyumaya](https://github.com/nyumaya/nyumaya_audio_recognition) for providing the excellent audio models and processing library.

Many thanks to the [Tensorflow](https://www.tensorflow.org) project for doing the speech processing heavy lifting.


### Installation

Hotword is cross compiled on a desktop and copied to the target rpi3b+.  As such, the build 
process will require some environment variables so it can find the various pieces.  The 
following is how my environment is setup:
```
# project directory
export RASPBIAN=~/your/workspace/raspbian
# project cross-compiler
export RASPBIANCROSS=$RASPBIAN/tools/arm-bcm2708/gcc-6-arm-linux-gnueabihf/armv6-rpi-linux-gnueabihf/bin/arm-linux-gnueabihf-
# TensorFlow location
export TFLOWSDK=$RASPBIAN/tensorflow
```

Get Hotword like this:
```
cd your/workspace/raspbian
git clone https://gitlab.com:tylerjbrooks/hotword.git

```

Get nyumaya audio recognition library like this:
```
cd your/workspace/raspbian
git clone https://gitlab.com:tylerjbrooks/nyumaya_audio_recognition_lib.git
cd nyumaya_audio_recognition_lib
git checkout rpi

```

Get nyumaya audio recognition models like this:
```
cd your/workspace/raspbian
git clone https://github.com/nyumaya/nyumaya_audio_recognition.git

```

Get Tensorflow like this:
```
cd your/workspace/raspbian
git clone https://gitlab.com:tylerjbrooks/tensorflow.git
cd tensorflow
git checkout rpi
cd ..

```

Get the Raspbian tool chain like this:
```
cd your/workspace/raspbian
git clone https://github.com/raspberrypi/tools.git
cd tools/arm-bcm2708
git clone https://github.com/africanmudking/gcc-6-arm-linux-gnueabihf.git
cd gcc-6-arm-linux-gnueabihf
cat armv6-rpi-linux-gnueabihf.tar.xz.parta* > armv6-rpi-linux-gnueabihf.tar.xz
tar xvJf armv6-rpi-linux-gnueabihf.tar.xz
cd ../../..
```

At this point, you should have all the software you need to build Hotword.

### Build Notes

Start by building Tensorflow Lite:
```
cd your/workspace/raspbian
cd tensorflow/tensorflow/lite/tools/make
./download_dependencies.sh
./cross_rpi_lib.sh
```

Build Nyumaya Audio Recognition Library
```
cd your/workspace/raspbian
cd nyumaya_audio_recognition_lib
make
```

Build Hotword:
```
cd your/workspace/raspbian
cd hotword
make
```

### Usage

Hotword requires the model and label file be downloaded to the rpib3+ separately.  Setup a 
deployment directory and download the model zip file like this:
```
# on the target rpi3b+
cd your/deployment/dir/hotword
mkdir models
cd models
# copy the nyumay_audio_recognition/models/Hotword/* to here
cd ..
# copy your hotword executable from your cross compile machine to here.
```

This is how you invoke hotword:
```

hotword -htydmls
version: 0.5

  where:
  (h)elp        = this screen
  (t)esttime    = test duration     (default = 30sec)
                = 0 to run until ctrl-c
  (y)ield time  = yield time        (default = 1000usec)
  (d)evice      = device name       (default = hw:1,0)
  (m)odel       = path to model     (default = ./models/alexa.tflite)
  (l)abels      = path to labels    (default = ./models/labelmap.txt)
  (s)ensitivity = model sensitivity (default = 0.5)
```

#### Simple Example

A typical example command would be:
```
./hotword -t 10
```

This command will capture audio for 10 seconds.  While it runs it will print out 'alexa'
every time it heres the hotword.  The output will look like this:

```
./hotword -t 10

Test Setup...
    test time: 10 seconds
       device: hw:1,0
        model: ./models/alexa.tflite
       lables: ./models/alexa_labels.txt
  sensitivity: 0.500000

      pid cmd: top -H -p 1164

............<alexa>.........<alexa>.........<alexa>....................

Capturer Results...
      read time (us): high:128657 avg:85047 low:80292 reads:100
   extract time (us): high:9972 avg:9851 low:9788 reads:100
    detect time (us): high:4690 avg:4377 low:4338 reads:100
     total test time: 10.138358 sec
   frames per second: 9.863530 fps


```
This indicates that the program heard the word 'alexa' three times.  The '.' are printed
while the program is listening.  On average, the program read 100 frames every 85ms.  It
took on average 9.8ms to extract the speech features and 4.4ms to detect the hotword.

Of special interest, is the 'top -H -p 1164' command which is printed just under the 'Test Setup'
section.  You can run this command in a seperate terminal window on your rpi3b+ to see what 
threads are created and how much of the CPU they are consuming.  The program prints out this 
command for each run for convenience.

### Discussion

Hotword is composed of a UI thread plus a worker thread.

- hotword.cpp:  UI thread.  It launches the other thread and goes to sleep for the 
duration of the test.
- capturer.{h,cpp}:  ALSA capture thread.  The main loop reads audio from the microphone
and submits it to the autio recognition library for analysis.

All the significate threads in the program are derived from a base state machine 
(base.{h,cpp}).  See the comment at the top of base.h for more details.

### Notes

### To Do

- Split off nyumaya library files from the program.
